import 'dart:math';

import 'package:flutter/material.dart';

class HourHandPainter extends CustomPainter {
  final Paint hourHandPaint;
  int hours;
  int minutes;

  HourHandPainter({required this.hours, required this.minutes})
      : hourHandPaint = Paint() {
    hourHandPaint.color = Colors.black87;
    hourHandPaint.style = PaintingStyle.fill;
  }

  @override
  void paint(Canvas canvas, Size size) {
    final double radius = size.width / 2;

    canvas.save();
    canvas.translate(radius, radius);

    double hoursGreater12 =
        2 * pi * ((this.hours - 12) / 12 + (this.minutes / 720));
    double hoursLess12 = 2 * pi * ((this.hours - 12) + (this.minutes / 720));

    canvas.rotate(this.hours >= 12 ? hoursGreater12 : hoursLess12);

    Path path = Path();
    _drawHeartShape(path, radius);
    _drawHourHand(path, radius);
    path.close();

    canvas.drawPath(path, hourHandPaint);
    canvas.drawShadow(path, Colors.black, 2.0, false);
    canvas.restore();
  }

  void _drawHeartShape(Path path, double radius) {
    path
      ..moveTo(0.0, -radius + 15.0)
      ..quadraticBezierTo(-3.5, -radius + 25.0, -15.0, -radius + (radius / 4))
      ..quadraticBezierTo(
          -20.0, -radius + radius / 3, -7.5, -radius + radius / 3)
      ..lineTo(0.0, -radius + radius / 4)
      ..lineTo(7.5, -radius + radius / 3)
      ..quadraticBezierTo(
          20.0, -radius + radius / 3, 15.0, -radius + radius / 4)
      ..quadraticBezierTo(3.5, -radius + 25.0, 0.0, -radius + 15.0);
  }

  void _drawHourHand(Path path, double radius) {
    path
      ..moveTo(-1.0, -radius + radius / 4)
      ..lineTo(-5.0, -radius + radius / 2)
      ..lineTo(-2.0, 0.0)
      ..lineTo(2.0, 0.0)
      ..lineTo(5.0, -radius + radius / 2)
      ..lineTo(1.0, -radius + radius / 4);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}
